package ru.nlmk.study.threadpool;

import org.junit.jupiter.api.Test;

import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.*;

class SimpleThreadpoolTest {
    @Test
    public void testSimpleThreadpool() throws Exception {
        int runnableCount = 30;
        SimpleThreadpool threadpool = SimpleThreadpool.getInstance();
        final AtomicInteger count = new AtomicInteger(0);
        Runnable r = new Runnable() {
            @Override
            public void run() {
                count.getAndIncrement();
            }
        };
        for (int i = 0; i < runnableCount; i++) {
            threadpool.execute(r);
        }
        threadpool.stop();
        threadpool.awaitTermination();
        assertEquals(runnableCount, count.get());
    }

    @Test
    public void testSimpleThreadpoolCustomThreadcount() throws Exception {
        int threadCount = 20;
        SimpleThreadpool threadpool = SimpleThreadpool.getInstance(threadCount);
        final AtomicInteger count = new AtomicInteger(0);
        Runnable r = new Runnable() {
            @Override
            public void run() {
                count.getAndIncrement();
                try {
                    Thread.sleep(200);
                } catch (Exception e) {
                    //Do nothing
                }
            }
        };
        for (int i = 0; i < threadCount*10; i++) {
            threadpool.execute(r);
        }
        threadpool.stop();
        threadpool.awaitTermination();
        assertEquals(threadCount*10, count.get());
    }

    @Test
    public void testAwaitTermination() throws Exception {
        int runnableCount = 6;
        SimpleThreadpool threadpool = SimpleThreadpool.getInstance(runnableCount / 2);
        final AtomicInteger count = new AtomicInteger(0);
        Runnable r = new Runnable() {
            @Override
            public void run() {
                count.getAndIncrement();
                try {
                    Thread.sleep(100);
                } catch (Exception e) {
                    //Do nothing
                }
            }
        };
        for (int i = 0; i < runnableCount; i++) {
            threadpool.execute(r);
        }
        threadpool.stop();
        threadpool.awaitTermination();
        assertEquals(runnableCount, count.get());
    }

    @Test
    public void testAwaitTerminationWithTimeout() throws Exception {
        int runnableCount = 10;
        SimpleThreadpool threadpool = SimpleThreadpool.getInstance(runnableCount / 2);
        final AtomicInteger count = new AtomicInteger(0);
        Runnable r = new Runnable() {
            @Override
            public void run() {
                count.getAndIncrement();
                try {
                    Thread.sleep(200);
                } catch (Exception e) {
                    //Do nothing
                }
            }
        };
        for (int i = 0; i < runnableCount; i++) {
            threadpool.execute(r);
        }
        threadpool.stop();
        // Threadpool should throw an exception here as the runnables cannot finish execution before the timeout
        assertThrows(TimeoutException.class, () -> threadpool.awaitTermination(300));
    }

    @Test
    public void testTerminate() throws Exception {
        int runnableCount = 10;
        SimpleThreadpool threadpool = SimpleThreadpool.getInstance(runnableCount / 2);
        final AtomicInteger count = new AtomicInteger(0);
        Runnable r = new Runnable() {
            @Override
            public void run() {
                count.getAndIncrement();
                try {
                    Thread.sleep(200);
                } catch (Exception e) {
                    //Do nothing
                }
            }
        };
        for (int i = 0; i < runnableCount; i++) {
            threadpool.execute(r);
        }
        threadpool.terminate();
        // Threadpool should terminate before all runnables are executed
        threadpool.awaitTermination(300);
        assertFalse(runnableCount == count.get());
    }
}
