package ru.nlmk.study.threadpool;

public class ThreadpoolException extends RuntimeException{
    public ThreadpoolException(Throwable cause) {
        super(cause);
    }
}
